export default defineAppConfig({
  alpine: {
    title: 'test',
    description: 'The minimalist blog theme',
    image: {
      src: '/social-card-preview.png',
      alt: '服务器记录站点',
      width: 400,
      height: 300
    },
    header: {
      position: 'right', // possible value are : | 'left' | 'center' | 'right'
      logo: {
        path: '/logo.svg', // path of the logo
        pathDark: '/logo-dark.svg', // path of the logo in dark mode, leave this empty if you want to use the same logo
        alt: '服务器记录站点' // alt of the logo
      }
    },
    footer: {
      credits: {
        enabled: true, // possible value are : true | false
        repository: '#' // our github repository
      },
      navigation: true, // possible value are : true | false
      alignment: 'center', // possible value are : 'none' | 'left' | 'center' | 'right'
      message: '站点驱动：' // string that will be displayed in the footer (leave empty or delete to disable)
    },
    socials: {
      Vercel: {
        icon: 'simple-icons:vercel',
        label: 'Vercel',
        href: 'https://vercel.com/',
      },
      Gitlab:{
        icon: 'simple-icons:gitlab',
        label: 'Gitlab',
        href: 'https://gitlab.com',
      },
      Nuxt:{
        icon: 'simple-icons:nuxtdotjs',
        label: 'Nuxt',
        href: 'https://nuxt.com',
      },
      Alpine:{
        icon: 'simple-icons:git',
        label: 'Alpine',
        href: 'https://github.com/nuxt-themes/alpine',
      }
    },
    form: {
      successMessage: 'Message sent. Thank you!'
    }
  }
})
