---
layout: default
head.title: 服务器记录
description: 记录我的世界服务器大事件发生情况。
title: 首页
---

::hero
---
image: '/alpine-0.webp'
---
#title
你好，这里是我的世界服务器记录站点。
#description
### 公示

[（2023年6月6日）](/articles/2023-6-6/)
- 第一次删档测试完成，计划进行第二次测试。  

[（2023年6月3日）](/articles/2023-6-3/)
- 服务器正式搭建完成，开始第一次删档测试。

::

### 这里是什么
这里是为了客观记录我的世界服务器的主要变更记而创建的记录站点。同时，这里会差时间人工更新服务器的重大事件。事件发生与记录时间最大相差不超过12小时。同时会在记录重大事件时，留下相关截图。

### 服务器基础内容公示
- 服务器地址：  
 首选：mc.kserks.top  
 备用：cat.mcla.fun:11594
- 服务器核心  
 *paper-1.19.4*   
 paper-1.15.2 
  
::gallery
---
images:
  - /alpine-0.webp
  - /alpine-1.webp
  - /alpine-2.webp
---
::
